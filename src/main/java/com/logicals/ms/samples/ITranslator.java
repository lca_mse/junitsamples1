// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

public interface ITranslator
{
  /**
   * translate.
   * 
   * @param fromLanguage
   *          ...
   * @param toLanguage
   *          ...
   * @param word
   *          (in from language) to be translated
   * @return word in toLanguage
   * @author Mario Semo (MS)
   * @version 1.0
   * @since 13-01-10 - 19:38:43
   */
  String translate(String fromLanguage, String toLanguage, String word);
}
