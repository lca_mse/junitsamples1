// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

/**
 * Greeting. With help of a Translator class creates a greeting string.
 * 
 * @author Mario Semo (MS)
 * @see <reference>
 * @version 1.0
 * @since 13-01-10 - 19:36:17
 */
public class Greeting
{
  private final transient ITranslator translator;

  /**
   * @param translator
   *          the translator to use
   */
  public Greeting(final ITranslator translator)
  {
    this.translator = translator;
  }

  /**
   * @param language
   *          language to say hello
   * @param name
   *          to whom say hello
   * @return the generated message
   */
  public final String sayHello(final String language, final String name)
  {
    return translator.translate("English", language, "Hello") + " " + name;
  }

}
