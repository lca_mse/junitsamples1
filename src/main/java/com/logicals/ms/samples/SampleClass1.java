// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

import java.io.*;
import java.util.Iterator;

/**
 * My Sample Class.
 *
 * @author Mario Semo (MS)
 * @see <reference>
 * @version 1.0
 * @since 13-01-10 - 18:46:53
 */
public class SampleClass1
{

  /**
   * @return 1
   */
  public final int barInt1()
  {
    // CHECKSTYLE:OFF
    return 1;
    // CHECKSTYLE:ON
  }

  /**
   * @param pVal
   *          ...
   * @return hallo + pVal
   */
  public final String barStringHallo(final String pVal)
  {
    return "hallo" + pVal;
  }

  public final void barException()
  {
    // ATTENTION: A RUNTIME exception is thrown here.
    throw new NumberFormatException("ungueltig");
  }

  /**
   * @return sum
   */
  @SuppressWarnings("PMD.SystemPrintln")
  public static int barAddAll(Iterator<?> iterator)
  {
     // CHECKSTYLE:OFF
    int sum = 0;
    // CHECKSTYLE:ON
    System.out.println("bar_add_all");
    while (iterator.hasNext())
    {
      final Object obj = iterator.next();
      System.out.println("..." + obj);
      sum += (Integer)obj;
    }
    return sum;
  }

  /**
   * @throws IOException
   *           Maybe throwing this exception, depending on osw.close().
   */
  public final void barWriteTo(final OutputStreamWriter osw) throws IOException
  {
    osw.close();
  }
}
