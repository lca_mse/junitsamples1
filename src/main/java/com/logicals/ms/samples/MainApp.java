// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

/**
 * Main Application Class.
 */
public final class MainApp
{
  /**
   * private CTOR.
   */
  private MainApp()
  {
  }

  /**
   * main.
   * 
   * @param args
   *          Command line arguments
   */
  @SuppressWarnings("PMD.SystemPrintln")
  public static void main(final String[] args)
  {
    System.out.println("Hello World!");
    final SampleClass1 aFoo = new SampleClass1();
    System.out.println(aFoo.barStringHallo("1") + aFoo.barInt1());
  }
}
