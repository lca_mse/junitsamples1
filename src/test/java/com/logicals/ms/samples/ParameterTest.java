// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.
package com.logicals.ms.samples;

import java.util.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.*;
import org.junit.runners.Parameterized.Parameters;

/**
 * parameterized tests
 * 
 * @author ms see http://www.mkyong.com/unittest/junit-4-tutorial-6-parameterized-test/
 */

@RunWith(Parameterized.class)
public class ParameterTest
{
  private int number;

  /**
   * @param number
   *          test number
   */
  public ParameterTest(int number)
  {
    this.number = number;
  }

  /**
   * @return Collection of parameters to test.
   */
  @Parameters
  public static Collection<Object[]> buildUpParametersForSetupTest()
  {
    Object[][] data = new Object[][] { { 1 }, { 1 }, { 2 }, { 3 }, { 5 } };
    return Arrays.asList(data);

  }

  /**
   * Test
   */
  @Test
  public void pushTest()
  {
    System.out.println("Parameterized Number is : " + number);
  }

}
