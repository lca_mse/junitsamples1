// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

// Das ist eine Testsuite fuer die FooTest und die GreetingsTest class!
// erzeugt mit:
// markiere beide Testclassen
// RightClick->New->Other->JAVA-Junit->Testsuite

package com.logicals.ms.samples;

import org.junit.runner.RunWith;
import org.junit.runners.*;
import org.junit.runners.Suite.SuiteClasses;

// Im folgenden Sample laeuft die Testsuite nur mit der Greeting Class.
// Wird die Testsuite ausgefuehrt, werden also nur Tests der GreetingsTestclass gemacht.
// Mit folgenden Statement
// { FooTest.class, GreetingTest.class })
// werden die Tests sowohl der FooTest.class als auch der GreetingTest.class gemacht.

@RunWith(Suite.class)
@SuiteClasses({ GreetingTest.class })
public class AllTests
{
  AllTests()
  {
    // Intentionaly empty CTOR.
  }
}
