// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

import org.junit.runner.*;
import org.junit.runner.notification.Failure;

public final class MyTestRunner
{
  private MyTestRunner()
  {
  }

  /**
   * @param args
   */
  @SuppressWarnings({ "PMD.SystemPrintln", "PMD.DataflowAnomalyAnalysis" })
  public static void main(final String[] args)
  {
    Result result = JUnitCore.runClasses(FooTest.class);
    for (Failure failure : result.getFailures())
    {
      System.out.println("**** " + failure.toString());
    }
  }
}