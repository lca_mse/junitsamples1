// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import java.io.*;
import java.util.Iterator;
import org.hamcrest.BaseMatcher;
import org.junit.*;

/**
 * @author ms
 */
public class FooTest
{
  private SampleClass1 aFoo;

  // -----------------------------------------------
  // setup and teardown Funktions
  // -----------------------------------------------

  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception
  {
         System.out.println("@BeforeClass method will be executed before JUnit test for a Class starts");
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception
  {
       System.out.println("@AfterClass method will be executed after JUnit test for a Class Completed");
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception
  {
       System.out.println("@Before method will execute before every JUnit4 test");
       aFoo = new SampleClass1();
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception
  {
       System.out.println("@After method will execute before every JUnit4 test");
       aFoo = null;
  }

  // -----------------------------------------------
  // simple junit tests
  // -----------------------------------------------

  @Test
  public void testBar1()
  {
    assertTrue(aFoo.barInt1() == 1);
  }

  @Test
  public void testBarStringHallo()
  {
    assertTrue("hallo1".equals(aFoo.barStringHallo("1")));
  }

  @Test(expected = NumberFormatException.class)
  public void testBarException()
  {
    aFoo.barException();
  }

  // -----------------------------------------------
  // MOCKITO SAMPLES
  // just for fun
  // -----------------------------------------------

  // This example creates a mock iterator and makes it return "Hello" the first time method next() is called.
  // Calls after that return "World". Then we can run normal assertions.
  @SuppressWarnings("rawtypes")
  @Test
  public void testIteratorWillReturnHelloWorld()
  {
    // arrange
    Iterator i = mock(Iterator.class);
    when(i.next()).thenReturn("Hello").thenReturn("World");

    // act
    String result = i.next() + " " + i.next();
    System.out.println("..." + result + "...");

    // TESTS
    assertEquals("Hello World", result);
  }

  @SuppressWarnings("rawtypes")
  @Test
  public void testBarAddAll()
  {
    // arrange
    Iterator i = mock(Iterator.class);
    when(i.next()).thenReturn(111).thenReturn(222);
    when(i.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);

    // TESTS
    assertEquals(SampleClass1.barAddAll(i), 333);
  }

  // Stubs can also return different values depending on arguments passed into the method. For example:
  @SuppressWarnings("unchecked")
  @Test
  public void testWithArguments1()
  {
    @SuppressWarnings("rawtypes")
    Comparable c = mock(Comparable.class);
    when(c.compareTo("Test")).thenReturn(1);
    // when(c.compareTo(anyInt())).thenReturn(-1);

    // TESTS
    assertEquals(1, c.compareTo("Test"));

    // should just test thing in one test!
    // assertEquals(-1,c.compareTo(123));
    // assertEquals(-1,c.compareTo(99));
  }

  // Stubs can also return different values depending on arguments passed into the method. For example:
  @SuppressWarnings("unchecked")
  @Test
  public void testWithArguments2()
  {
    @SuppressWarnings("rawtypes")
    Comparable c = mock(Comparable.class);
    when(c.compareTo(anyInt())).thenReturn(-1);

    // TESTS
    assertEquals(-1, c.compareTo(123));
    assertEquals(-1, c.compareTo(99));
  }

  // This example throws an IOException when the mock OutputStream close method is called.
  // We verify easily that the OutputStreamWriter rethrows the exception of the wrapped output stream.
  @Test(expected = IOException.class)
  public void testOutputStreamWriterRethrowsAnExceptionFomOutputStream() throws IOException
  {
    OutputStream mock = mock(OutputStream.class);
    OutputStreamWriter osw = new OutputStreamWriter(mock);
    doThrow(new IOException()).when(mock).close();

    // TESTS
    aFoo.barWriteTo(osw);
  }

  // To verify actual calls to underlying objects (typical mock object usage),
  // we can use verify(mock_object).method_call; For example:
  @Test
  public void testOutputStreamWriterClosesOutputStreamOnClose() throws IOException
  {
    OutputStream mock = mock(OutputStream.class);
    OutputStreamWriter osw = new OutputStreamWriter(mock);

    // TESTS
    aFoo.barWriteTo(osw);

    // verifications
    verify(mock).close();
  }

  // You can use arguments on methods and matchers such as anyInt() similar to the previous example.
  // Note that you cant mix literals and matchers, so if you have multiple arguments they all have to
  // be either literals or matchers. use eq(value) matcher to convert a literal into a matcher that
  // compares on value. Mockito comes with lots of matchers already built in, but sometimes you need
  // a bit more flexibility. For example, OutputStreamWriter will buffer output and then send it to
  // the wrapped object when flushed, but we dont know how big the buffer is upfront. So we cant
  // use equality matching. However, we can supply our own matcher:
  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Test
  public void testOutputStreamWriterBuffersAndForwardsToOutputStream() throws IOException
  {
    // Setup
    OutputStream mock = mock(OutputStream.class);
    OutputStreamWriter osw = new OutputStreamWriter(mock);

    // ... RUN CODE .... (should be real code in class Foo. )
    osw.write('a');
    osw.flush();

    // Verification of what is written
    // can't do this as we don't know how long the array is going to be
    // verify(mock).write(new byte[]{'a'},0,1);

    BaseMatcher arrayStartingWithA = new BaseMatcher()
    {
      @Override
      public void describeTo(org.hamcrest.Description description)
      {
        // nothing
      }

      // check that first character is A
      @Override
      public boolean matches(Object item)
      {
        byte[] actual = (byte[])item;
        return actual[0] == 'a';
      }
    };

    // check that first character of the array is A, and that the other two arguments are 0 and 1
    verify(mock).write((byte[])argThat(arrayStartingWithA), eq(0), eq(1));
  }
}
