// Copyright (C) logi.cals automation solutions and services GmbH. All rights reserved.

package com.logicals.ms.samples;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import org.junit.Test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * Greeting Test via Mockito (mocking for ITranslator).
 *
 * @author Mario Semo (MS)
 * @see <reference>
 * @version 1.0
 * @since 13-01-10 - 19:40:56
 */
public class GreetingTest
{

  // ------------------------------------
  // setup and tear down
  // ------------------------------------
  @BeforeClass
  public static void setUpBeforeClass() throws Exception
  {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception
  {
  }

  @Before
  public void setUp() throws Exception
  {
  }

  @After
  public void tearDown() throws Exception
  {
  }

  // ------------------------------------
  // test
  // ------------------------------------
  @Test
  public void shouldTestGreetingInItalian()
  {
    ITranslator mockTranslator = mock(ITranslator.class);

    Greeting greeting = new Greeting(mockTranslator);

    stub(mockTranslator.translate("English", "Italian", "Hello")).toReturn("Ciau");

    // execute greeting sayHello and assert the created output
    String expected = "Ciau Paulo";
    String actual = greeting.sayHello("Italian", "Paulo");
    assertEquals("Objects aren't identical", expected, actual);

    // verify the mocked translator was called with English,Italian,Hello!!
    verify(mockTranslator).translate("English", "Italian", "Hello");
  }
}
