@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: build mit / ohne unit Tests
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/
@echo off
setlocal

set MVN_OPT_BUILD_SKIP_TEST="-Dmaven.test.skip"

if /i (%1)==(junit) SET MVN_OPT_BUILD_SKIP_TEST=

call mvn %MVN_OPT_BUILD_SKIP_TEST% package

@rem call mvn %MVN_OPT_BUILD_SKIP_TEST% clean install || exit /b 1

endlocal
