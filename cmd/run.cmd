@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: run
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/
@echo off
setlocal

set _VERSION=1.0.0-SNAPSHOT

if (%LC32_AUTHOR_SHORT%)==() goto :TryUserName
goto :ok
:TryUserName
if (%USERNAME%)==() goto :error2
SET LC32_AUTHOR_SHORT%USERNAME%
goto :ok

:ok

call :getP %CD%
echo --%_CURRDIRNAME%--

echo java -cp target/%_CURRDIRNAME%-%_VERSION%.jar com.logicals.%LC32_AUTHOR_SHORT%.samples.App %*
java -cp target/%_CURRDIRNAME%-%_VERSION%.jar com.logicals.%LC32_AUTHOR_SHORT%.samples.App %*
goto :ende

:error
echo no projekt name
goto :ende

:error2
echo weder LC32_AUTHOR_SHORT noch USERNAME gesetzt.
goto :ende

:getP
set _CURRDIRNAME=%~n1
goto :EOF

:ende
endlocal
