@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: Junit 4 Style Tests, using MOCKITO Framework
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/

different samples for unit testing with JUNIT 4 w/o help of mockito

for eclipse use mvn eclipse:eclipse

for command line simple use cmd\build (with/out junit as arg1)
